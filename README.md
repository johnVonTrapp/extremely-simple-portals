# Extremely Simple Portals

Extremely Simple Portals, ESP, is a barebones, straight-forward spigot plugin that links portals together. There's no fancy wand, addressing system, ability to use different blocks, etc. It just links portals together.