package com.johnvontrapp.esp.linkmode;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.command.CommandResult;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import com.johnvontrapp.esp.ESPPlugin;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LinkModeTest {
	private static ServerMock server;
	private static LinkModeCommand linkModeCommand;

	@BeforeAll
	static void setUp() {
		server = MockBukkit.mock();
		MockBukkit.load(ESPPlugin.class);
		linkModeCommand = (LinkModeCommand) server.getPluginCommand("link-mode").getExecutor();
	}

	@Test
	void startLinkCreationMode() {
		PlayerMock player = server.addPlayer();

		player.performCommand("link-mode");

		player.assertSaid("You are now in link mode.");
		player.assertSaid("Please walk into the starting portal.");

		assertTrue(linkModeCommand.getLinkModeSaveBehavior().isLinkModeEnabled(player), "Link mode is disabled when it should be enabled.");
	}

	@Test
	void endLinkCreationMode() {
		PlayerMock player = server.addPlayer();

		linkModeCommand.getLinkModeSaveBehavior().enableLinkMode(player);
		assertTrue(linkModeCommand.getLinkModeSaveBehavior().isLinkModeEnabled(player), "Link mode could not be enabled for test!");

		player.performCommand("link-mode");

		player.assertSaid("You are no longer in link mode.");

		assertFalse(linkModeCommand.getLinkModeSaveBehavior().isLinkModeEnabled(player), "Link mode is enabled when it should be disabled.");
	}

	@Test
	void nonPlayerLinkCreationMode() {
		CommandResult result = server.executeConsole("link-mode");
		result.assertFailed();
		result.assertResponse("Only players can enter link mode!");
	}

	@AfterAll
	static void tearDown() {
		MockBukkit.unmock();
	}
}
