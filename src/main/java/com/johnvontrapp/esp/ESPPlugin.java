package com.johnvontrapp.esp;

import com.johnvontrapp.esp.linkmode.LinkModeCommand;
import com.johnvontrapp.esp.linkmode.LinkModeHashSet;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import java.io.File;

public class ESPPlugin extends JavaPlugin {

	public ESPPlugin() {
		super();
	}

	protected ESPPlugin(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file) {
		super(loader, description, dataFolder, file);
	}

	@Override
	public void onEnable() {
		this.getCommand("link-mode").setExecutor(new LinkModeCommand(new LinkModeHashSet()));

		getLogger().info("Extremely Simple Portals is enabled!");
	}

	@Override
	public void onDisable() {
		getLogger().info("Extremely Simple Portals is disabled!");
	}
}
