package com.johnvontrapp.esp.linkmode;

import org.bukkit.command.CommandSender;

public interface LinkModeSaveBehavior {
	public boolean isLinkModeEnabled(CommandSender commandSender);

	public void enableLinkMode(CommandSender commandSender);

	public void disableLinkMode(CommandSender commandSender);
}
