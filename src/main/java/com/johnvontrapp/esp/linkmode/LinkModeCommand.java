package com.johnvontrapp.esp.linkmode;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LinkModeCommand implements CommandExecutor {
	private final LinkModeSaveBehavior linkMode;

	public LinkModeCommand(LinkModeSaveBehavior linkMode) {
		this.linkMode = linkMode;
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
		if (commandSender instanceof Player) {
			if (linkMode.isLinkModeEnabled(commandSender)) {
				linkMode.disableLinkMode(commandSender);
				commandSender.sendMessage("You are no longer in link mode.");
			} else {
				linkMode.enableLinkMode(commandSender);
				commandSender.sendMessage("You are now in link mode.");
				commandSender.sendMessage("Please walk into the starting portal.");
			}
		} else {
			commandSender.sendMessage("Only players can enter link mode!");
			return false;
		}
		return true;
	}

	public LinkModeSaveBehavior getLinkModeSaveBehavior() {
		return linkMode;
	}
}
