package com.johnvontrapp.esp.linkmode;

import org.bukkit.command.CommandSender;

import java.util.HashSet;
import java.util.Set;

public class LinkModeHashSet implements LinkModeSaveBehavior {
	Set<CommandSender> linkMode = new HashSet<>();

	@Override
	public boolean isLinkModeEnabled(CommandSender commandSender) {
		return linkMode.contains(commandSender);
	}

	@Override
	public void enableLinkMode(CommandSender commandSender) {
		linkMode.add(commandSender);
	}

	@Override
	public void disableLinkMode(CommandSender commandSender) {
		linkMode.remove(commandSender);
	}
}
